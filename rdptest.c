#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <SDL2/SDL.h>

#include <winpr/crt.h>

#include <freerdp/freerdp.h>

#include "rdp.h"
#include "rdpuppet.h"
#include "scancode.h"

int rdpuppet_handle_sdl2_event(
	struct rdpuppet_context *puppet_context,
	SDL_Event *event
);

int main(int argc, char *argv[])
{
	const int nclient = 1;
	struct rdpuppet_context *rdpc[nclient];
	
	if (nclient * 2 > 64) {
		printf("Max number of clients is 32.\n");
		exit(1);
	}
	
	SDL_Init(SDL_INIT_VIDEO);
	for (int i = 0; i < nclient; i++) {
		rdpc[i] = rdpuppet_new();
		rdpuppet_parse_command_line(rdpc[i], argc, argv);
		rdpuppet_start(rdpc[i]);
	}
	
	HANDLE handles[64];
	DWORD handle_index = 0, status = 0;
	for (int i = 0; i < nclient; i++) {
		handles[handle_index++] = rdpc[i]->client_thread;
		handles[handle_index++] = rdpc[i]->input_thread;
	}
	
	//bool quit = false;
	//SDL_Event event;
	//while (!quit) {
	//	if (SDL_WaitEvent(&event)) {
	//		quit = rdpuppet_handle_sdl2_event(rdpc[0], &event);
	//	}
	//}
	
	//  Wait for all threads to exit; in this case, the input thread will
	// trigger a disconnect via rdpuppet_stop.
	//  Third argument should be true. Waiting for all threads is a better idea
	// than waiting for one to error.
	status = WaitForMultipleObjects(
			handle_index, handles, false, INFINITE
	);
	
	for (int i = 0; i < nclient; i++) {
		//rdpuppet_stop(rdpc[i]);
		rdpuppet_free(rdpc[i]);
	}
	return status != WAIT_FAILED;
}

int rdpuppet_handle_sdl2_event(
	struct rdpuppet_context *puppet_context,
	SDL_Event *event
)
{
	switch (event->type) {
	case SDL_QUIT:
		return true;
	case SDL_WINDOWEVENT: {
		printf("Window %d.\n", event->window.windowID);
		switch (event->window.type) {
			case SDL_WINDOWEVENT_CLOSE:
				break;
		}
		break;
	}
	case SDL_KEYDOWN: {
		rdpuppet_send_key_event(puppet_context, rdpuppet_key_down,
			true, event->key.keysym.scancode
		);
		break;
	}
	case SDL_KEYUP: {
		rdpuppet_send_key_event(puppet_context, rdpuppet_key_up,
			false, event->key.keysym.scancode
		);
		break;
	}
	case SDL_MOUSEMOTION: {
		rdpuppet_send_mouse_event(puppet_context, rdpuppet_mouse_move,
			false, event->motion.x, event->motion.y
		);
		break;
	}
	case SDL_MOUSEBUTTONDOWN: 
	case SDL_MOUSEBUTTONUP: {
		UINT16 ex = 0, flags = 0; 
		
		//  Button up event is done by clearing PTR_FLAGS_DOWN but
		// leaving the button flag set.
		//  For now PTR_FLAGS_DOWN and PTR_XFLAGS_DOWN are the same;
		// if they were not, we would need to handle that here.
		if (event->button.state == SDL_PRESSED)
			flags |= PTR_FLAGS_DOWN;
		
		switch (event->button.button) {
		case SDL_BUTTON_LEFT:
			flags |= PTR_FLAGS_BUTTON1; break;
		case SDL_BUTTON_RIGHT:
			flags |= PTR_FLAGS_BUTTON2; break;
		case SDL_BUTTON_MIDDLE:
			flags |= PTR_FLAGS_BUTTON3; break;
		case SDL_BUTTON_X1:
			ex = 1;
			flags |= PTR_XFLAGS_BUTTON1; break;
		case SDL_BUTTON_X2:
			ex = 1;
			flags |= PTR_XFLAGS_BUTTON2; break;
		}
	
		rdpuppet_send_mouse_event_raw(puppet_context,
			ex, flags, event->button.x, event->button.y
		);
		break;
	}
	case SDL_MOUSEWHEEL: {
		UINT16 flags = 0;

		//  The code below assumes that horizontal and vertical
		// scrolling are not folded into the same event. The code will
		// not work if they are as there is only one flag for sign.
		if (event->wheel.x < 0 || event->wheel.y < 0)
			flags |= PTR_FLAGS_WHEEL_NEGATIVE;

		if (event->wheel.x)
			flags |= PTR_FLAGS_HWHEEL;

		if (event->wheel.y)
			flags |= PTR_FLAGS_WHEEL;

		//  Note that the absolute value of the scroll distance is not
		// taken.
		// TODO: Is PTR_FLAGS_WHEEL_NEGATIVE required?
		rdpuppet_send_mouse_event_raw(puppet_context,
			false, flags, event->wheel.x, event->wheel.y
		);
		break;
	}
	}
	return false;
}
