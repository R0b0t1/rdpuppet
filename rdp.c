#include "rdp.h"

BOOL rdpuppet_global_init()
{
	printf("rdpuppet_global_init\n");
	if (freerdp_handle_signals() != 0)
		return FALSE;
	return TRUE;
}

void rdpuppet_global_uninit()
{
	printf("rdpuppet_global_uninit\n");
}

BOOL rdpuppet_client_new(freerdp *instance, rdpContext *context)
{
	printf("rdpuppet_client_new\n");
	
	instance->ContextNew = rdpuppet_context_new;
	instance->ContextFree = rdpuppet_context_free;
	instance->PreConnect = rdpuppet_preconnect;
	instance->PostConnect = rdpuppet_postconnect;
	instance->PostDisconnect = rdpuppet_postdisconnect;
	instance->Authenticate = rdpuppet_authenticate;
	
	return 1;
}

void rdpuppet_client_free(freerdp *instance, rdpContext *context)
{
	printf("rdpuppet_client_free\n");
}

int rdpuppet_client_start(rdpContext *context)
{
	printf("rdpuppet_client_start\n");
	return 0;
}

int rdpuppet_client_stop(rdpContext *context)
{
	printf("rdpuppet_client_stop\n");
	freerdp_abort_connect(context->instance);
	return 0;
}

BOOL rdpuppet_context_new(freerdp *instance, rdpContext *context)
{
	printf("rdpuppet_context_new\n");
	return 1;
}

void rdpuppet_context_free(freerdp *instance, rdpContext *context)
{
	printf("rdpuppet_context_free\n");
}

BOOL rdpuppet_preconnect(freerdp *instance)
{
	printf("rdpuppet_preconnect\n");
	
	if ((PubSub_SubscribeChannelConnected(
		instance->context->pubSub,
		rdpuppet_channel_connected)) != CHANNEL_RC_OK)
		return FALSE;
	
	if ((PubSub_SubscribeChannelDisconnected(
		instance->context->pubSub,
		rdpuppet_channel_disconnected)) != CHANNEL_RC_OK)
		return FALSE;

	/*
	if (freerdp_client_load_addins(instance->context->channels,
		instance->settings))
		return FALSE;
	*/

	rdpSettings *settings;
	BOOL bitmap_cache;

	if (!instance)
		return FALSE;

	settings = instance->settings;

	if (!settings || !settings->OrderSupport)
		return FALSE;

	bitmap_cache = settings->BitmapCacheEnabled;
	settings->OrderSupport[NEG_DSTBLT_INDEX] = TRUE;
	settings->OrderSupport[NEG_PATBLT_INDEX] = TRUE;
	settings->OrderSupport[NEG_SCRBLT_INDEX] = TRUE;
	settings->OrderSupport[NEG_OPAQUE_RECT_INDEX] = TRUE;
	settings->OrderSupport[NEG_DRAWNINEGRID_INDEX] = FALSE;
	settings->OrderSupport[NEG_MULTIDSTBLT_INDEX] = FALSE;
	settings->OrderSupport[NEG_MULTIPATBLT_INDEX] = FALSE;
	settings->OrderSupport[NEG_MULTISCRBLT_INDEX] = FALSE;
	settings->OrderSupport[NEG_MULTIOPAQUERECT_INDEX] = TRUE;
	settings->OrderSupport[NEG_MULTI_DRAWNINEGRID_INDEX] = FALSE;
	settings->OrderSupport[NEG_LINETO_INDEX] = TRUE;
	settings->OrderSupport[NEG_POLYLINE_INDEX] = TRUE;
	settings->OrderSupport[NEG_MEMBLT_INDEX] = bitmap_cache;
	settings->OrderSupport[NEG_MEM3BLT_INDEX] = TRUE;
	settings->OrderSupport[NEG_MEMBLT_V2_INDEX] = bitmap_cache;
	settings->OrderSupport[NEG_MEM3BLT_V2_INDEX] = FALSE;
	settings->OrderSupport[NEG_SAVEBITMAP_INDEX] = FALSE;
	settings->OrderSupport[NEG_GLYPH_INDEX_INDEX] = TRUE;
	settings->OrderSupport[NEG_FAST_INDEX_INDEX] = TRUE;
	settings->OrderSupport[NEG_FAST_GLYPH_INDEX] = TRUE;
	settings->OrderSupport[NEG_POLYGON_SC_INDEX] = FALSE;
	settings->OrderSupport[NEG_POLYGON_CB_INDEX] = FALSE;
	settings->OrderSupport[NEG_ELLIPSE_SC_INDEX] = FALSE;
	settings->OrderSupport[NEG_ELLIPSE_CB_INDEX] = FALSE;

	return TRUE;
}

BOOL rdpuppet_postconnect(freerdp *instance)
{
	printf("rdpuppet_postconnect\n");

	if (!gdi_init(instance, PIXEL_FORMAT_RGBA32))
		return FALSE;

	if (!rdpuppet_register_pointer(instance->context->graphics))
		return FALSE;

	instance->update->BeginPaint = rdpuppet_begin_paint;
	instance->update->EndPaint = rdpuppet_end_paint;
	instance->update->DesktopResize = rdpuppet_resize;
	pointer_cache_register_callbacks(instance->update);

	return TRUE;
}

void rdpuppet_postdisconnect(freerdp *instance)
{
	printf("rdpuppet_postdisconnect\n");
	gdi_free(instance);
}

BOOL rdpuppet_authenticate(
	freerdp *instance,
	char **username, char **password, char **domain
)
{
	printf("rdpuppet_authenticate\n");
	return 1;
}

void rdpuppet_channel_connected(void *context, ChannelConnectedEventArgs *e)
{
	printf("rdpuppet_channel_connected\n");
}

void rdpuppet_channel_disconnected(
	void *context, ChannelDisconnectedEventArgs *e
)
{
	printf("rdpuppet_channel_disconnected\n");
}

BOOL rdpuppet_register_pointer(rdpGraphics *graphics)
{
	printf("rdpuppet_register_pointer\n");

	rdpPointer pointer;

	if (!graphics)
		return FALSE;

	pointer.size = sizeof(rdpPointer);
	pointer.New = rdpuppet_pointer_new;
	pointer.Free = rdpuppet_pointer_free;
	pointer.Set = rdpuppet_pointer_set;
	pointer.SetNull = rdpuppet_pointer_setnull;
	pointer.SetDefault = rdpuppet_pointer_setdefault;
	pointer.SetPosition = rdpuppet_pointer_setposition;
	graphics_register_pointer(graphics, &pointer);
	return TRUE;
}

BOOL rdpuppet_pointer_new(rdpContext *context, rdpPointer *pointer)
{
	printf("rdpuppet_pointer_new\n");
	if (!context || !context->gdi || !pointer)
		return FALSE;
	return TRUE;
}

void rdpuppet_pointer_free(rdpContext *context, rdpPointer *pointer)
{
	printf("rdpuppet_pointer_free\n");
	if (!context || !pointer)
		return;
}

BOOL rdpuppet_pointer_set(rdpContext *context, const rdpPointer *pointer)
{
	printf("rdpuppet_pointer_set\n");
	if (!context)
		return FALSE;
	return TRUE;
}

BOOL rdpuppet_pointer_setnull(rdpContext *context)
{
	printf("rdpuppet_pointer_setnull\n");
	if (!context)
		return FALSE;
	return TRUE;
}

BOOL rdpuppet_pointer_setdefault(rdpContext *context)
{
	printf("rdpuppet_pointer_setdefault\n");
	if (!context)
		return FALSE;
	return TRUE;
}

BOOL rdpuppet_pointer_setposition(rdpContext *context, UINT32 x, UINT32 y)
{
	printf("rdpuppet_pointer_setposition\n");
	if (!context)
		return FALSE;
	return TRUE;
}
