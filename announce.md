[SIZE=6][B]RDPuppet - Library for Creating RDP Sessions[/B][/SIZE]
	RDPuppet uses FreeRDP to create Remote Desktop client connections to a
computer for automatic remote administration. The input devices associated with
the client connection are made available to send input to the server. Typically
this computer is running Microsoft Windows, though it is possible to run
[COLOR=Grey][FONT=Courier New]xrdp[/FONT][/COLOR] on Linux/X11 (or other programs on other hosts).

[SIZE=5][B]Contents[/B][/SIZE]
[LIST=1]
[*]Introduction.
[*]Contents.
[*]Download.
[*]Usage.
	[LIST=1][*]Caveats.
	[*]In Progress.[/LIST]
[*]Development.
	[LIST=1][*]Language Bindings.
	[*]Linux
	[*]Windows.
		[LIST=1][*]I want to build everything.
		[*]I want to modify RDPuppet.[/LIST][/LIST]
[*]Justification.
[/LIST]

[SIZE=5][B]Download.[/B][/SIZE]

[SIZE=5][B]Usage.[/B][/SIZE]
	When using RDPuppet it is best to immediately launch a program after
connecting and logging in. This allows whatever you are running to continue
running in a disconnected but still active session, and prevents latency from
affecting the command stream. You will likely also want to install [url=https://github.com/stascorp/rdpwrap]RDPWrap[/url].

	See [COLOR=Grey][FONT=Courier New]rdptest.c[/FONT][/COLOR]. Invoke:
[CODE]./rdpuppet /u:Username /v:127.0.0.1[/CODE]
	The demonstration accepts all arguments that FreeRDP does. When using
RDPuppet in a noninteractive manner you likely want to call [COLOR=Grey][FONT=Courier New]rdpuppet_setup[/FONT][/COLOR]
with the hostname, username, and password.

[SIZE=4][B]Caveats.[/B][/SIZE]
	RDPuppet doesn't yet support encryption. You should not use it to connect to
machines over a WAN with no other precautions. If you must connect over a WAN
use either a VPN or SOCKS tunnel.

[SIZE=4][B]In Progress.[/B][/SIZE]
[LIST=1]
[*]HID muting (to prevent your input from messing with the session).
[*]Window hiding.
[*]Screen searching.
[*]Video decoding pause/resume.
[*]Encryption, incl. tutorial for proper server setup.
[*]Proper multithreading support.
[/LIST]

[SIZE=5][B]Development.[/B][/SIZE]
[SIZE=4][B]Language Bindings.[/B][/SIZE]
	No bindings are available at this time. As of 8/21/2018 two languages are
planned: LAPE (for use with Simba) and Python. The general procedure for
creating OOP bindings is as follows.

[LIST=1]
	[*]Translate the symbolic constants in [COLOR=Grey][FONT=Courier New]rdpuppet.h[/FONT][/COLOR] that deal with input,
namely:
[HIGHLIGHT=C]...[/HIGHLIGHT]
	[*]Define [COLOR=Grey][FONT=Courier New]struct rdpuppet_context *[/FONT][/COLOR] to be [COLOR=Grey][FONT=Courier New]void *[/FONT][/COLOR] or [COLOR=Grey][FONT=Courier New]Pointer[/FONT][/COLOR]. The
FreeRDP context in particular is gigantic and probably not worth porting.
FreeRDP has a huge number of functions that you do not need to implement.
	[*]Add FFI definitions for the following functions (full definition omitted for
brevity, check [COLOR=Grey][FONT=Courier New]rdpuppet.h[/FONT][/COLOR]):
[HIGHLIGHT=C]rdpuppet_new
rdpuppet_parse_command_line
rdpuppet_setup
rdpuppet_start
rdpuppet_stop
rdpuppet_free

rdpuppet_send_key_event_raw
rdpuppet_send_key_event
rdpuppet_send_mouse_event[/HIGHLIGHT][/LIST]

[SIZE=4][B]Linux.[/B][/SIZE]
Install the following packages and their development headers:
[CODE]sdl2 winpr2 freerdp2[/CODE]

Run [COLOR=Grey][FONT=Courier New]make[/FONT][/COLOR] and optionally [COLOR=Grey][FONT=Courier New]make rdptest[/FONT][/COLOR].

[SIZE=4][B]Windows.[/B][/SIZE]
	To prevent recreating an ad-hoc package system on Windows the build process
currently relies on [COLOR=Grey][FONT=Courier New]vcpkg[/FONT][/COLOR] and MSVC++/Visual Studio 2017.

[SIZE=3][B]I want to build everything.[/B][/SIZE]
	Clone [COLOR=Grey][FONT=Courier New]vcpkg[/FONT][/COLOR] from [url]https://github.com/Microsoft/vcpkg[/url]. Use it to install
the following, ensuring they are x64:
[CODE]sdl2:x64-windows winpr2:x64-windows freerdp2:x64-windows[/CODE]
	Use [COLOR=Grey][FONT=Courier New]vcpkg[/FONT][/COLOR] to create the dependency archive and continue as if you had
just downloaded the archive.

[SIZE=3][B]I want to modify RDPuppet.[/B][/SIZE]
	Download the archive of the dependencies.
TODO.

[SIZE=5][B]Justification.[/B][/SIZE]
	Window's remote administration facilities are a combination of primitive,
brittle, and quick to change. It does not make much sense to invest in e.g.
PowerShell remoting for server management when that interface is liable to
change and can not even support common programs. Perhaps more problematic is
the notion of interactive and noninteractive sessions built into Windows and
its administration APIs. This notion prevents programs that are properly
running as a daemon from using various system resources. Programs which are not
"properly" daemonized and run in the default interactive session require that
session to be active and must share it with any other program which must run
this way.
	For this reason it was decided the most generic and reusable way to automate
the administration of a Windows machine was to connect to it via RDP. RDP is
not necessary, any interface implementing the remote desktop services API will
do, however RDP is the only known option with widespread use.
	That RDP is supported on other platforms as well is helpful for automating
(GUI) programs in a cross platform manner.