#pragma once

#define DEBUG
#ifdef DEBUG
#	define DEBUG_PRINT(...) \
		do { printf(__VA_ARGS__); } while (0);
#else
#	define DEBUG_PRINT(...) \
		do { } while(0);
#endif

enum rdpuppet_input_event_type {
	rdpuppet_key = 1,
	rdpuppet_mouse
};

enum rdpuppet_key_event_type {
	rdpuppet_key_down = 1,
	rdpuppet_key_up
};

enum rdpuppet_mouse_event_type {
	rdpuppet_mouse_move = 1,
	rdpuppet_mouse_down,
	rdpuppet_mouse_up
};

enum rdpuppet_mouse_button {
	rdpuppet_mouse_button1 = 1,
	rdpuppet_mouse_button2,
	rdpuppet_mouse_button3
};

struct rdpuppet_key_event {
	enum rdpuppet_key_event_type ke_type;
};

struct rdpuppet_mouse_event {
	enum rdpuppet_mouse_event_type me_type;
};

struct rdpuppet_input_event {
	enum rdpuppet_input_event_type ev_type;
	union {
		struct rdpuppet_key_event key;
		struct rdpuppet_mouse_event mouse;
	};
};

struct rdpuppet_context {
	// The rdpContext structure is included at the beginning to embed it inside
	// a larger structure. This might not be necessary but is the safest
	// option.
	rdpContext rdp_context;

	//  Due to difficulties integrating the objects FreeRDP and WinPR use for
	// kernel-mediated wakeups with non-Windows OSes a thread is created per
	// rdpuppet context to maintain the RDP connection. Input events are passed
	// to this thread via synchronized collection provided by WinPR.
	HANDLE client_thread;
	HANDLE input_thread;
	wMessageQueue *input_messages;

	SDL_Window *window;
	SDL_Texture *texture;
	SDL_Renderer *renderer;

	bool running; // Check to see if already in rdpContext.
	bool visible;
	bool draw_screen;
	bool accept_input;
};

struct rdpuppet_context *rdpuppet_new();
int rdpuppet_parse_command_line(
	struct rdpuppet_context *puppet_context,
	int argc, char *argv[]
);
int rdpuppet_setup(
	struct rdpuppet_context *puppet_context,
	char *hostname,
	char *username,
	char *password
);
int rdpuppet_start(struct rdpuppet_context *puppet_context);
int rdpuppet_stop(struct rdpuppet_context *puppet_context);
void rdpuppet_free(struct rdpuppet_context *puppet_context);

int rdpuppet_push_input_event(
	struct rdpuppet_context *puppet_context,
	struct rdpuppet_input_event *input_event
);
int rdpuppet_send_key_event_raw(
	struct rdpuppet_context *puppet_context,
	enum rdpuppet_key_event_type type,
	bool down, int scancode
);
int rdpuppet_send_key_event(
	struct rdpuppet_context *puppet_context,
	enum rdpuppet_key_event_type type,
	bool down, int scancode
);
int rdpuppet_send_mouse_event_raw(
	struct rdpuppet_context *puppet_context,
	bool ex, int flags, int x, int y
);
int rdpuppet_send_mouse_event(
	struct rdpuppet_context *puppet_context,
	enum rdpuppet_mouse_event_type type,
	bool down, int x, int y
);
