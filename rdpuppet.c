#include <stdio.h>
#include <assert.h>

#include <SDL2/SDL.h>

#include <winpr/crt.h>
#include <winpr/synch.h>
#include <winpr/thread.h>
#include <winpr/collections.h>

#include <freerdp/freerdp.h>
#include <freerdp/api.h>
#include <freerdp/event.h>
#include <freerdp/client.h>
#include <freerdp/input.h>
#include <freerdp/scancode.h>
#include <freerdp/settings.h>
#include <freerdp/gdi/gdi.h>
#include <freerdp/utils/signal.h>
#include <freerdp/client/cmdline.h>
#include <freerdp/cache/pointer.h>
#include <freerdp/channels/channels.h>

#include "rdp.h"
#include "scancode.h"
#include "rdpuppet.h"

DWORD WINAPI rdpuppet_client_thread(LPVOID param);
DWORD WINAPI rdpuppet_input_thread(LPVOID param);

BOOL rdpuppet_begin_paint(rdpContext *context);
BOOL rdpuppet_end_paint(rdpContext *context);
BOOL rdpuppet_resize(rdpContext *context);

struct rdpuppet_context *rdpuppet_new()
{
	printf("rdpuppet_new\n");
	//  TODO: Check to see that this struct is copied and does not improperly
	// go out of scope.
	//  This should be FREERDP_CLIENT_ENTRY_POINTS but the type resolves
	// erroneously to int (not defined?).
	struct rdp_client_entry_points_v1 fcep = {
		.Size = sizeof(struct rdp_client_entry_points_v1),
		.Version = 1,

		.GlobalInit = rdpuppet_global_init,
		.GlobalUninit = rdpuppet_global_uninit,

		.ContextSize = sizeof(struct rdpuppet_context),
		.ClientNew = rdpuppet_client_new,
		.ClientFree = rdpuppet_client_free,
		.ClientStart = rdpuppet_client_start,
		.ClientStop = rdpuppet_client_stop
	};

	struct rdpuppet_context *r =
		(struct rdpuppet_context *)freerdp_client_context_new(&fcep);
	assert(r);
	return r;
}

int rdpuppet_parse_command_line(
	struct rdpuppet_context *puppet_context,
	int argc, char *argv[]
)
{
	printf("rdpuppet_parse_command_line\n");

	rdpSettings *rds = puppet_context->rdp_context.settings;
	int r = 0;
	if ((r = freerdp_client_settings_parse_command_line(rds, argc, argv, 0)))
		printf("freerdp_client_settings_parse_command_line\n");
	freerdp_client_settings_command_line_status_print(rds, r, argc, argv);
	return r;
}

int rdpuppet_setup(
	struct rdpuppet_context *puppet_context,
	char *hostname,
	char *username,
	char *password
)
{
	rdpSettings *rds = puppet_context->rdp_context.settings;
	rds->ServerHostname = hostname;
	rds->Username = username;
	rds->Password = password;
	return 0;
}

int rdpuppet_start(struct rdpuppet_context *puppet_context)
{
	printf("rdpuppet_start\n");
	
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Texture *texture;
	
	freerdp_client_start((rdpContext *)puppet_context);
	freerdp_connect(puppet_context->rdp_context.instance);

	//SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("RDPuppet",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		1024, 768, 0);
	renderer = SDL_CreateRenderer(window, -1, 0);
	texture = SDL_CreateTexture(renderer,
		SDL_PIXELFORMAT_ABGR8888,
		SDL_TEXTUREACCESS_TARGET | SDL_TEXTUREACCESS_STREAMING,
		1024, 768);
	
	puppet_context->window = window;
	puppet_context->renderer = renderer;
	puppet_context->texture = texture;
	puppet_context->client_thread = CreateThread(
		NULL, 0, 
		rdpuppet_client_thread, (void *)puppet_context,
		0, NULL
	);
	assert(puppet_context->client_thread);
	puppet_context->input_thread = CreateThread(
		NULL, 0, 
		rdpuppet_input_thread, (void *)puppet_context,
		0, NULL
	);
	assert(puppet_context->input_thread);
	return 0;
}

int rdpuppet_stop(struct rdpuppet_context *puppet_context)
{
	printf("rdpuppet_stop\n");
	freerdp_disconnect(puppet_context->rdp_context.instance);
	SDL_DestroyTexture(puppet_context->texture);
	SDL_DestroyRenderer(puppet_context->renderer);
	SDL_DestroyWindow(puppet_context->window);
	return 0;
}

void rdpuppet_free(struct rdpuppet_context *puppet_context)
{
	printf("rdpuppet_free\n");
	freerdp_client_context_free((rdpContext *)puppet_context);
}

int rdpuppet_send_key_event_raw(
	struct rdpuppet_context *puppet_context,
	enum rdpuppet_key_event_type type,
	bool down, int scancode
)
{
	return freerdp_input_send_keyboard_event_ex(
		puppet_context->rdp_context.instance->input,
		!down, scancode
	);
}

int rdpuppet_send_key_event(
	struct rdpuppet_context *puppet_context,
	enum rdpuppet_key_event_type type,
	bool down, int scancode
)
{
	return rdpuppet_send_key_event_raw(
		puppet_context,	type,
		down, rdpuppet_scancode_sdl2_to_dinput(scancode)
	);
}

int rdpuppet_send_mouse_event_raw(
	struct rdpuppet_context *puppet_context,
	bool ex, int flags, int x, int y
)
{
	if (!ex)
		return freerdp_input_send_mouse_event(
			puppet_context->rdp_context.instance->input,
			flags, x, y
		);
	else
		return freerdp_input_send_extended_mouse_event(
			puppet_context->rdp_context.instance->input,
			flags, x, y
		);
}

int rdpuppet_send_mouse_event(
	struct rdpuppet_context *puppet_context,
	enum rdpuppet_mouse_event_type type,
	bool down, int x, int y
)
{
	return 1;
}

DWORD WINAPI rdpuppet_client_thread(LPVOID param)
{
	printf("rdpuppet_client_thread\n");

	struct rdpuppet_context *rdpc =
		(struct rdpuppet_context *)param;
	rdpContext *context = &rdpc->rdp_context;
	freerdp *instance = context->instance;

	HANDLE handles[64];
	while(!freerdp_shall_disconnect(instance)) {
		DWORD status = WAIT_FAILED, n = 0, handle_index = 0;

		//  TODO: Add the input event queue's event to the waitable objects.
		// Cache this value.
		// It looks like no locking is required - leave this out?
		//handles[handle_index++] = Queue_Event(rdpc->input_messages);

		n = freerdp_get_event_handles(
			context, &handles[handle_index], 64 - handle_index
		);
		// TODO: n == 0 is an error, handle it.
		handle_index += n;
		
		status = WaitForMultipleObjects(
			handle_index, handles, false, INFINITE
		);
		// TODO: status == WAIT_FAILED is an error, handle it.
		if (status == WAIT_FAILED) {
			break;
		}

		if (!freerdp_check_event_handles(context)) {
			// TODO: Autoreconnect?
			break;
		}
	}

	ExitThread(0);
	return 0;
}

DWORD WINAPI rdpuppet_input_thread(LPVOID param)
{
	struct rdpuppet_context *rdpc =
		(struct rdpuppet_context *)param;
	rdpContext *rdc = &rdpc->rdp_context;
	freerdp *rdi = rdc->instance;
	
	bool quit = false;
	SDL_Event event;
	
	while (!quit) {
		if (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			quit = true;
			break;
		case SDL_WINDOWEVENT: {
			// TODO: Break this out into a function.
			switch (event.window.event) {
			case SDL_WINDOWEVENT_CLOSE: {
				quit = true;
				break;
			}
			case SDL_WINDOWEVENT_EXPOSED: {
				break;
			}
			}
			break;
		}
		case SDL_KEYDOWN: {
			//rdpuppet_send_key_event(rdpc, rdpuppet_key_down,
			//	true, event.key.keysym.scancode
			//);
			freerdp_input_send_keyboard_event_ex(rdi->input, true,
				rdpuppet_scancode_sdl2_to_dinput(event.key.keysym.scancode)
			);
			break;
		}
		case SDL_KEYUP: {
			//rdpuppet_send_key_event(rdpc, rdpuppet_key_up,
			//	false, event.key.keysym.scancode
			//);
			freerdp_input_send_keyboard_event_ex(rdi->input, false,
				rdpuppet_scancode_sdl2_to_dinput(event.key.keysym.scancode)
			);
			break;
		}
		case SDL_MOUSEMOTION: {
			freerdp_input_send_mouse_event(rdi->input,
				PTR_FLAGS_MOVE, event.motion.x, event.motion.y
			);
			break;
		}
		case SDL_MOUSEBUTTONDOWN: 
		case SDL_MOUSEBUTTONUP: {
			UINT16 ex = 0, flags = 0; 
			
			//  Button up event is done by clearing PTR_FLAGS_DOWN but
			// leaving the button flag set.
			//  For now PTR_FLAGS_DOWN and PTR_XFLAGS_DOWN are the same;
			// if they were not, we would need to handle that here.
			if (event.button.state == SDL_PRESSED)
				flags |= PTR_FLAGS_DOWN;
			
			switch (event.button.button) {
			case SDL_BUTTON_LEFT:
				flags |= PTR_FLAGS_BUTTON1; break;
			case SDL_BUTTON_RIGHT:
				flags |= PTR_FLAGS_BUTTON2; break;
			case SDL_BUTTON_MIDDLE:
				flags |= PTR_FLAGS_BUTTON3; break;
			case SDL_BUTTON_X1:
				ex = 1;
				flags |= PTR_XFLAGS_BUTTON1; break;
			case SDL_BUTTON_X2:
				ex = 1;
				flags |= PTR_XFLAGS_BUTTON2; break;
			}

			if (!ex)
				freerdp_input_send_mouse_event(rdi->input,
					flags, event.button.x, event.button.y
				);
			else
				freerdp_input_send_extended_mouse_event(rdi->input,
					flags, event.button.x, event.button.y
				);
			break;
		}
		case SDL_MOUSEWHEEL: {
			UINT16 flags = 0;

			//  The code below assumes that horizontal and vertical
			// scrolling are not folded into the same event. The code will
			// not work if they are as there is only one flag for sign.
			if (event.wheel.x < 0 || event.wheel.y < 0)
				flags |= PTR_FLAGS_WHEEL_NEGATIVE;

			if (event.wheel.x)
				flags |= PTR_FLAGS_HWHEEL;

			if (event.wheel.y)
				flags |= PTR_FLAGS_WHEEL;

			//  Note that the absolute value of the scroll distance is not
			// taken.
			// TODO: Is PTR_FLAGS_WHEEL_NEGATIVE required?
			freerdp_input_send_mouse_event(rdi->input,
				flags, event.wheel.x, event.wheel.y
			);
			break;
		}
		}
		}

		const struct rdpuppet_context *rdpc =
			(const struct rdpuppet_context *)param;
		rdpGdi *rdg = rdpc->rdp_context.gdi;
		SDL_Texture *texture = rdpc->texture;
		SDL_Renderer *renderer = rdpc->renderer;

		SDL_LockTexture(texture, NULL,
			(void **)&rdg->primary->bitmap->data,
			(int *)&rdg->primary->bitmap->scanline);
		SDL_UpdateTexture(texture, NULL,
			rdg->primary->bitmap->data,
			rdg->primary->bitmap->scanline);
		//SDL_UnlockTexture(texture);
		SDL_RenderCopy(renderer, texture, NULL, NULL);
		SDL_RenderPresent(renderer);
	}
	
	// TODO: Qualify this with some conditional?
	rdpuppet_stop(rdpc);
	
	ExitThread(0);
	return 0;
}

BOOL rdpuppet_begin_paint(rdpContext *context)
{
	printf("rdpuppet_begin_paint\n");
	
	if (!context)
		return false;
	
	rdpGdi *gdi = context->gdi;
	if (!gdi || !gdi->primary || !gdi->primary->hdc)
		return false;
	
	HGDI_WND hwnd = gdi->primary->hdc->hwnd;
	if (!hwnd || !hwnd->invalid)
		return false;
	
	hwnd->invalid->null = true;
	hwnd->ninvalid = 0;

	return true;

	// For rdpuppet. ---------------------------------
	const struct rdpuppet_context *rdpc =
		(const struct rdpuppet_context *)context;
	rdpGdi *rdg = rdpc->rdp_context.gdi;
	SDL_Texture *texture = rdpc->texture;
	SDL_Renderer *renderer = rdpc->renderer;

	SDL_LockTexture(texture, NULL,
		(void **)&rdg->primary->bitmap->data,
		(int *)&rdg->primary->bitmap->scanline);
	SDL_UpdateTexture(texture, NULL,
		rdg->primary->bitmap->data,
		rdg->primary->bitmap->scanline);
	//SDL_UnlockTexture(texture);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer);

	return true;
}

BOOL rdpuppet_end_paint(rdpContext *context)
{
	printf("rdpuppet_end_paint\n");
	
	if (!context->instance)
		return false;
	
	rdpSettings *settings = context->instance->settings;
	if (!settings)
		return false;
	
	rdpGdi *gdi = context->gdi;
	if (!gdi || !gdi->primary || !gdi->primary->hdc)
		return false;
	
	HGDI_WND hwnd = gdi->primary->hdc->hwnd;
	if (!hwnd)
		return false;
	
	int ninvalid = hwnd->ninvalid;
	if (ninvalid == 0)
		return true;
	
	HGDI_RGN cinvalid = hwnd->cinvalid;
	if (!cinvalid)
		return false;
	
	int x1, y1, x2, y2;
	x1 = cinvalid[0].x;
	y1 = cinvalid[0].y;
	x2 = cinvalid[0].x + cinvalid[0].w;
	y2 = cinvalid[0].y + cinvalid[0].h;
	
	for (int i = 0; i < ninvalid; i++) {
		x1 = MIN(x1, cinvalid[i].x);
		y1 = MIN(y1, cinvalid[i].y);
		x2 = MAX(x2, cinvalid[i].x + cinvalid[i].w);
		y2 = MAX(y2, cinvalid[i].y + cinvalid[i].h);
	}
	return true;
}

BOOL rdpuppet_resize(rdpContext *context)
{
	printf("rdpuppet_resize\n");
	
	if (!context || !context->instance || !context->settings)
		return false;
	return true;
}
