#pragma once

#include <winpr/crt.h>

#include <freerdp/freerdp.h>
#include <freerdp/api.h>
#include <freerdp/event.h>
#include <freerdp/client.h>
#include <freerdp/input.h>
#include <freerdp/scancode.h>
#include <freerdp/settings.h>
#include <freerdp/gdi/gdi.h>
#include <freerdp/utils/signal.h>
#include <freerdp/client/cmdline.h>
#include <freerdp/cache/pointer.h>
#include <freerdp/channels/channels.h>

BOOL rdpuppet_global_init();
void rdpuppet_global_uninit();
BOOL rdpuppet_client_new(freerdp *instance, rdpContext *context);
void rdpuppet_client_free(freerdp *instance, rdpContext *context);
int rdpuppet_client_start(rdpContext *context);
int rdpuppet_client_stop(rdpContext *context);

BOOL rdpuppet_context_new(freerdp *instance, rdpContext *context);
void rdpuppet_context_free(freerdp *instance, rdpContext *context);

BOOL rdpuppet_preconnect(freerdp *instance);
BOOL rdpuppet_postconnect(freerdp *instance);
void rdpuppet_postdisconnect(freerdp *instance);
BOOL rdpuppet_authenticate(
	freerdp *instance,
	char **username, char **password, char **domain
);

void rdpuppet_channel_connected(void *context, ChannelConnectedEventArgs *e);
void rdpuppet_channel_disconnected(
	void *context, ChannelDisconnectedEventArgs *e
);

BOOL rdpuppet_register_pointer(rdpGraphics *graphics);
BOOL rdpuppet_pointer_new(rdpContext *context, rdpPointer *pointer);
void rdpuppet_pointer_free(rdpContext *context, rdpPointer *pointer);
BOOL rdpuppet_pointer_set(rdpContext *context, const rdpPointer *pointer);
BOOL rdpuppet_pointer_setnull(rdpContext *context);
BOOL rdpuppet_pointer_setdefault(rdpContext *context);
BOOL rdpuppet_pointer_setposition(rdpContext *context, UINT32 x, UINT32 y);

BOOL rdpuppet_begin_paint(rdpContext *context);
BOOL rdpuppet_end_paint(rdpContext *context);
BOOL rdpuppet_resize(rdpContext *context);
