CC=gcc
CF=-std=gnu11 -Wall -g -fPIC

INCS=\
	$(shell pkg-config --cflags sdl2 winpr2 freerdp-client2)

LIBS=\
	$(shell pkg-config --libs sdl2 winpr2 freerdp2 freerdp-client2)

.phony: all clean

all: rdptest

rdptest: rdptest.o rdpuppet.o rdp.o
	$(CC) $(CF) $^ -o $@ $(INCS) $(LIBS)

rdptest.o: rdptest.c
	$(CC) $(CF) $< -c -o $@ $(INCS) $(LIBS)
	
librdpuppet.a: rdpuppet.o rdp.o
	ar rcs $@ $^
	#$(CC) $(CF) $^ -o $@ $(INCS) $(LIBS)

rdpuppet.o: rdpuppet.c rdpuppet.h
	$(CC) $(CF) $< -c -o $@ $(INCS) $(LIBS)

rdp.o: rdp.c rdp.h
	$(CC) $(CF) $< -c -o $@ $(INCS) $(LIBS)

clean:
	rm librdpuppet.so *.o
